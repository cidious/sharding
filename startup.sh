#!/bin/bash

U_ID=`id -u`
G_ID=`id -g`
CURRENT_USER=$U_ID:$G_ID

export CURRENT_USER

docker-compose up -d
sleep 6s

docker-compose exec pg psql -U sms -f /home/root.sql
docker-compose exec pgusa psql -U sms -c "CREATE TABLE sms_usa (country CHAR(2), msisdn_from VARCHAR(100),msisdn_to VARCHAR(100), content TEXT);"
docker-compose exec pgrussia psql -U sms -c "CREATE TABLE sms_russia (country CHAR(2), msisdn_from VARCHAR(100),msisdn_to VARCHAR(100), content TEXT);"
docker-compose exec pggermany psql -U sms -c "CREATE TABLE sms_germany (country CHAR(2), msisdn_from VARCHAR(100),msisdn_to VARCHAR(100), content TEXT);"
docker-compose exec pg psql -U sms -c "INSERT INTO sms (country) (SELECT (array['US','RU','DE','UK']::varchar[])[(random()*10000)::INT%4+1] FROM generate_series(1,100));"

xdg-open 'http://127.0.0.1:5080/?pgsql=pg&username=sms&db=sms&ns=public'

