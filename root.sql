CREATE EXTENSION postgres_fdw;
CREATE SERVER pgusa FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'pgusa', dbname 'sms');
CREATE SERVER pgrussia FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'pgrussia', dbname 'sms');
CREATE SERVER pggermany FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'pggermany', dbname 'sms');
CREATE USER MAPPING FOR sms SERVER pgusa OPTIONS (user 'sms', password 'sms');
CREATE USER MAPPING FOR sms SERVER pgrussia OPTIONS (user 'sms', password 'sms');
CREATE USER MAPPING FOR sms SERVER pggermany OPTIONS (user 'sms', password 'sms');
IMPORT FOREIGN SCHEMA public LIMIT TO (sms_usa) FROM SERVER pgusa INTO public;
IMPORT FOREIGN SCHEMA public LIMIT TO (sms_russia) FROM SERVER pgrussia INTO public;
IMPORT FOREIGN SCHEMA public LIMIT TO (sms_germany) FROM SERVER pggermany INTO public;
CREATE TABLE sms (country CHAR(2), msisdn_from VARCHAR,msisdn_to VARCHAR, content TEXT) PARTITION BY LIST(country);
CREATE FOREIGN TABLE sms_usa PARTITION OF sms FOR VALUES IN ('US') SERVER pgusa;
CREATE FOREIGN TABLE sms_russia PARTITION OF sms FOR VALUES IN ('RU') SERVER pgrussia;
CREATE FOREIGN TABLE sms_germany PARTITION OF sms FOR VALUES IN ('DE') SERVER pggermany;
CREATE TABLE sms_others PARTITION OF sms DEFAULT;

