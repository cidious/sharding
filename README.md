# sharding

postgresql sharding example in docker containers

usage:

    git clone git@gitlab.com:cidious/sharding.git
    cd sharding
    ./startup.sh


When everything finishes, the browser opens the adminer. the host is 'pg', user, password and db are 'sms'.

Then take a look at 'sms' partition table and foreign tables: sms_usa, sms_russia, sms_germany.

All records that have 'US' in the country fies, go to sms_usa table in pgusa host.

The same is for 'RU' and 'DE'.

All the rest goes to sms_others table.

